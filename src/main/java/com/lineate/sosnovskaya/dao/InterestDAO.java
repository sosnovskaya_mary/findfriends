package com.lineate.sosnovskaya.dao;

import com.lineate.sosnovskaya.models.Interest;
import com.lineate.sosnovskaya.models.Interests;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface InterestDAO extends CrudRepository<Interest, Integer> {
    @Query(value = "SELECT i.userId FROM Interest i WHERE i.interest = :interest AND i.description = :description")
    Integer[] findUserIdByInterestAndDescription(@Param("interest") Interests interest,
                                                 @Param("description") String description);

    @Query(value = "SELECT user_id FROM interests WHERE description IN (SELECT description FROM interests WHERE user_id = :userId) \n" +
            "AND interest IN (SELECT interest FROM interests WHERE user_id = :userId) AND user_id <> :userId ", nativeQuery = true)
    Integer[] findByUserInterests(@Param("userId") int userId);
}
