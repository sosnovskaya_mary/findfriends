package com.lineate.sosnovskaya.dao;

import com.lineate.sosnovskaya.exception.FindFriendsException;
import com.lineate.sosnovskaya.models.User;
import com.lineate.sosnovskaya.utils.JDBCUtils;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static com.lineate.sosnovskaya.utils.JDBCUtils.*;

@Repository
public class UserDAO {
    private static final String URL = "jdbc:mysql://localhost:3306/findfriends";
    private static final String USER = "root";
    private static final String PASSWORD = "root";

    public String register(User user) throws FindFriendsException {
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            con.setAutoCommit(false);
            insertUser(con, user);
            String token = addActiveUser(con, user.getLogin());
            con.commit();
            return token;
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new FindFriendsException(ex);
        }
    }

    public User getUserByLogin(String login) throws FindFriendsException {
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            return JDBCUtils.getUserByLogin(con, login);
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new FindFriendsException(ex);
        }
    }

    public List<User> getUserById(Integer[] ids) {
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            return JDBCUtils.getUsersByIds(con, ids);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return Collections.emptyList();
        }
    }

    public String addActiveUser(String login) throws FindFriendsException{
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            return addActiveUser(con, login);
        }  catch (SQLException ex) {
            ex.printStackTrace();
            throw new FindFriendsException(ex);
        }
    }

    public String addActiveUser(Connection con, String login) throws SQLException {
        String token = UUID.randomUUID().toString();
        insertUserSession(con, login, token);
        return token;
    }

    public void removeActiveUser(String token) throws FindFriendsException {
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            removeUserSessionByToken(con, token);
        }  catch (SQLException ex) {
            ex.printStackTrace();
            throw new FindFriendsException(ex);
        }
    }

}
