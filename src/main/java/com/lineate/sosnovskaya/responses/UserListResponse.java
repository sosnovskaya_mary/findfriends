package com.lineate.sosnovskaya.responses;

import com.lineate.sosnovskaya.models.User;

import java.util.List;

public class UserListResponse implements Response {
    private List<User> users;

    public UserListResponse(List<User> users) {
        this.users = users;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
