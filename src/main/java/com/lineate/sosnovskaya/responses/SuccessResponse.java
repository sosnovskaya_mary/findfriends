package com.lineate.sosnovskaya.responses;

public class SuccessResponse implements Response {
    private String status = "success";

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
