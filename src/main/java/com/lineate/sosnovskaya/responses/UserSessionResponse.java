package com.lineate.sosnovskaya.responses;

public class UserSessionResponse implements Response {
    private String token;

    public UserSessionResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
