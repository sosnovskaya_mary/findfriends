package com.lineate.sosnovskaya.services;

import com.lineate.sosnovskaya.dao.UserDAO;
import com.lineate.sosnovskaya.exception.FindFriendsException;
import com.lineate.sosnovskaya.responses.ErrorResponse;
import com.lineate.sosnovskaya.models.User;
import com.lineate.sosnovskaya.responses.Response;
import com.lineate.sosnovskaya.responses.SuccessResponse;
import com.lineate.sosnovskaya.responses.UserSessionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserDAO userDAO;

    public Response register(User user) {
        try {
            String token = userDAO.register(user);
            return new UserSessionResponse(token);
        } catch (FindFriendsException ex) {
            return new ErrorResponse(ex.getMessage());
        }

    }

    public Response login(String login, String password) {
       try {
           User user = userDAO.getUserByLogin(login);
           if (password.equals(user.getPassword())) {
               String token = userDAO.addActiveUser(user.getLogin());
               return new UserSessionResponse(token);
           }
           return new ErrorResponse("wrong password");
       } catch (FindFriendsException ex) {
           return new ErrorResponse(ex.getMessage());
       }
    }

    public Response logout(String token) {
        try {
            userDAO.removeActiveUser(token);
            return new SuccessResponse();
        } catch (FindFriendsException ex) {
           return new ErrorResponse(ex.getMessage());
        }
    }
}
