package com.lineate.sosnovskaya.services;

import com.lineate.sosnovskaya.dao.InterestDAO;
import com.lineate.sosnovskaya.dao.UserDAO;
import com.lineate.sosnovskaya.exception.FindFriendsException;
import com.lineate.sosnovskaya.models.Interest;
import com.lineate.sosnovskaya.models.User;
import com.lineate.sosnovskaya.responses.Response;
import com.lineate.sosnovskaya.responses.SuccessResponse;
import com.lineate.sosnovskaya.responses.UserListResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class InterestService {

    @Autowired
    private InterestDAO interestDAO;

    @Autowired
    private UserDAO userDAO;

    public Response addInterest(List<Interest> interests) {
        interestDAO.saveAll(interests);
        return new SuccessResponse();
    }

    public Response deleteInterest(List<Interest> interests) {
        interestDAO.deleteAll(interests);
        return new SuccessResponse();
    }

    public Response findUsersByInterests(Interest interest) {
        Integer[] userIds = interestDAO.findUserIdByInterestAndDescription(
                interest.getInterest(), interest.getDescription());
        List<User> users = userDAO.getUserById(userIds);
        return new UserListResponse(users);
    }

    public Response findByUserInterests(User user) {
        Integer[] userIds = interestDAO.findByUserInterests(user.getId());
        List<User> users = userDAO.getUserById(userIds);
        return new UserListResponse(users);
    }
}
