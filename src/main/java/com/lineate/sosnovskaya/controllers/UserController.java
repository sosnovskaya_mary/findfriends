package com.lineate.sosnovskaya.controllers;

import com.lineate.sosnovskaya.models.User;
import com.lineate.sosnovskaya.models.UserSession;
import com.lineate.sosnovskaya.responses.Response;
import com.lineate.sosnovskaya.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public Response register(@RequestBody User user) {
        return userService.register(user);
    }

    @PostMapping("/login")
    public Response login(@RequestBody User user) {
        return userService.login(user.getLogin(), user.getPassword());
    }

    @PostMapping("/logout")
    public Response logout(@RequestBody UserSession userSession) {
        return userService.logout(userSession.getToken());
    }

}
