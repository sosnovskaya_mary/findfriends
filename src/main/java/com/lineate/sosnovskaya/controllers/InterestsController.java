package com.lineate.sosnovskaya.controllers;

import com.lineate.sosnovskaya.models.Interest;
import com.lineate.sosnovskaya.models.User;
import com.lineate.sosnovskaya.responses.Response;
import com.lineate.sosnovskaya.services.InterestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/interest")
public class InterestsController {

    @Autowired
    private InterestService interestService;

    @PostMapping("/add")
    public Response addInterests(@RequestBody List<Interest> interests) {
        return interestService.addInterest(interests);
    }

    @PostMapping("/delete")
    public Response deleteInterest(@RequestBody List<Interest> interests) {
        return interestService.deleteInterest(interests);
    }

    @PostMapping("/find")
    public Response findByInterest(@RequestBody Interest interest) {
        return interestService.findUsersByInterests(interest);
    }

    @PostMapping("/friends")
    public Response findByUserInterests(@RequestBody User user) {
        return interestService.findByUserInterests(user);
    }

}
