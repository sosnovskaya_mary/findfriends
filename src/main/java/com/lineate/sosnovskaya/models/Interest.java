package com.lineate.sosnovskaya.models;

import javax.persistence.*;

@Entity
@Table(name = "interests")
public class Interest {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "user_id")
    private int userId;

    @Column(name = "interest")
    @Enumerated(EnumType.STRING)
    private Interests interest;

    @Column(name = "description")
    private String description;

    public Interest() {
    }

    public Interest(int id, int userId, Interests interest, String description) {
        this.id = id;
        this.userId = userId;
        this.interest = interest;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Interests getInterest() {
        return interest;
    }

    public void setInterest(Interests interest) {
        this.interest = interest;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
