package com.lineate.sosnovskaya.models;

public enum Interests {
    FOOD, ART, PROFESSIONAL_FIELD, TRAVEL, BOOKS, MOVIES, MUSIC;
}
