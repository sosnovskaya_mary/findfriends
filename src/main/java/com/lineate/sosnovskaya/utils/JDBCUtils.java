package com.lineate.sosnovskaya.utils;

import com.lineate.sosnovskaya.models.User;

import java.sql.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class JDBCUtils {

    private static final String insertUserQuery = "INSERT into users values(?,?,?,?)";
    private static final String insertUserSessionQuery = "INSERT into user_sessions values(?,?,?)";
    private static final String getUserByLoginQuery = "SELECT * FROM users WHERE login = ";
    private static final String getUserByIdsQuery = "SELECT * FROM users WHERE id in ";
    private static final String getEmailsByIdsQuery = "SELECT email FROM users WHERE id in ";
    private static final String removeUserSessionByToken = "DELETE FROM user_sessions WHERE token = ?";

    public static boolean loadDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return true;
        } catch (ClassNotFoundException e) {
            System.out.println("Error loading JDBC Driver ");
            e.printStackTrace();
            return false;
        }
    }

    public static void insertUser(Connection con, User user) throws SQLException {
        try (PreparedStatement stmt = con.prepareStatement(insertUserQuery)) {
            stmt.setNull(1, java.sql.Types.INTEGER);
            stmt.setString(2, user.getLogin());
            stmt.setString(3, user.getPassword());
            stmt.setString(4, user.getEmail());
            stmt.executeUpdate();
        }
    }

    public static void insertUserSession(Connection con, String login, String token) throws SQLException {
        try (PreparedStatement stmt = con.prepareStatement(insertUserSessionQuery)) {
            stmt.setNull(1, java.sql.Types.INTEGER);
            stmt.setString(2, login);
            stmt.setString(3, token);
            stmt.executeUpdate();
        }
    }

    public static User getUserByLogin(Connection con, String loginForQuery) throws SQLException {
        User user = null;
        try (Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(getUserByLoginQuery + "\"" + loginForQuery + "\"")) {
            while (rs.next()) {
                int id = rs.getInt("id");
                String login = rs.getString("login");
                String password = rs.getString("password");
                String email = rs.getString("email");
                user = new User(id, login, password, email);
            }
        }
        return user;
    }

    public static List<User> getUsersByIds(Connection con, Integer[] idsForQuery) throws SQLException {
        if (idsForQuery == null) {
            return Collections.emptyList();
        }
        List<User> users = new LinkedList<>();
        try (Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(getUserByIdsQuery +
                     Arrays.toString(idsForQuery)
                             .replace("[", "(").replace("]", ")"))) {
            while (rs.next()) {
                int id = rs.getInt("id");
                String login = rs.getString("login");
                String password = rs.getString("password");
                String email = rs.getString("email");
                users.add(new User(id, login, password, email));
            }
        }
        return users;
    }

    public static void removeUserSessionByToken(Connection con, String token) throws SQLException {
        try (PreparedStatement statement = con.prepareStatement(removeUserSessionByToken)) {
            statement.setString(1, token);
            statement.execute();
        }
    }
}
