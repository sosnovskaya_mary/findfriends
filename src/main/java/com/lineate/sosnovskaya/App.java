package com.lineate.sosnovskaya;

import com.lineate.sosnovskaya.utils.JDBCUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class App {

    public static void main(String[] args) {
        if (JDBCUtils.loadDriver()) {
            SpringApplication.run(App.class);
        }
    }
}
