package com.lineate.sosnovskaya.exception;

public class FindFriendsException extends Exception {
    public FindFriendsException(Exception ex) {
        super(ex);
    }
}
